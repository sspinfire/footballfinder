# Prova tècnica DXC

## Objectius

L’objectiu de la prova és avaluar els seguents coneixements tècnics:
* Desenvolupament amb control de versions: Git
* Fonaments de la llibrería UIKit
* Implementació de crides asíncrones amb URLSession
* Estructuració i escalabilitat del codi
* Testing

## Descripció

La prova consistirà en desenvolupar una petita aplicació on podrem buscar
jugadors de futbo mitjançant llibreríes natives del sistema. Per fer-ho es
realitzaran petición contra la api https://sportmonks.com/football-api
L’aplicació tindrà dues pantalles:

## Pantalla de cerca

Aquesta pantalla estarà continguda en un navigationController y tindrà una
barra de cerca desenvolupada amb UISearchBarController. Els resultats de la
cerca s’hauràn de mostrar en la mateixa pantalla en forma de taula.
Cada cela de la taula tindrà una imatge circular a la part esquerra de 50x50 i el
nom del jugador a la dreta i sota el nom la seva edat actual.

## Pantalla de detall

En aquesta pantalla es mostrarà el detall del jugador i dades referents al seu
equip.
El detall contindrà:
* Nom abrejuat del jugador en forma de títol
* Foto del jugador
* Nom complert del jugador
* Nom del seu equip
* Escut del seu equip
* Altres dades que es vulguin afegir

## Llicència
[MIT](https://choosealicense.com/licenses/mit/)