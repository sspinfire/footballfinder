//
//  ManagerConnections.swift
//  FootballFinder
//
//  Created by manuel on 05/11/2020.
//

import Foundation
import UIKit

class ManagerConnections{
    var listPlayer: (([Player])->([Player]))?
    var team: ((DataTeam)->(DataTeam))?
    
    /// Function that make a GET to list all player that they have same name
    /// - Parameter namePlayer: Name of the football player
    func getDataPlayers(namePlayer: String){
        let session = URLSession.shared
        guard let url = URL(string: "\(Constants.URL.main)\(Constants.Endpoint.player)\(namePlayer)\(Constants.Endpoint.apiToken)\(Constants.Tokens.apiSport)") else{return}
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            guard error == nil else {return}
            guard let data = data else {return}
            do {
                 let decoder = JSONDecoder()
                 let response = try decoder.decode(PlayerData.self, from: data)
                _ = self.listPlayer!(response.data)

            } catch {
                print(error)
            }
        })
        task.resume()
    }
    /// Function that make a GET to give the team that have this id
    /// - Parameter idTeam: id of the team
    func getDataTeam(idTeam: String){
        let session = URLSession.shared
        guard let url = URL(string: "\(Constants.URL.main)\(Constants.Endpoint.team)\(idTeam)\(Constants.Endpoint.apiToken)\(Constants.Tokens.apiSport)") else{return}
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            guard error == nil else {return}
            guard let data = data else {return}
            do {
                 let decoder = JSONDecoder()
                 let response = try decoder.decode(Team.self, from: data)
                _ = self.team!(response.data)

            } catch {
                print(error)
            }
        })
        task.resume()
    }
}
