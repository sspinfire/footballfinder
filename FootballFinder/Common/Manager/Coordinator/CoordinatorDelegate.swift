//
//  CoordinatorDelegate.swift
//  FootballFinder
//
//  Created by manuel on 05/11/2020.
//

import Foundation
import UIKit
protocol  CoordinatorDelegate{
    static func instantiate() -> Self
}
extension CoordinatorDelegate where Self: UIViewController{
    static func instantiate() -> Self{
        let id = String(describing: self)
        switch id {
        case "DetailViewViewController":
            let viewController = DetailViewViewController(nibName: id, bundle: nil)
            return viewController as! Self
        default:
            let viewController = MainViewController(nibName: id, bundle: nil)
            return viewController as! Self
        }
    }
}
