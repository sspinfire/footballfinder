//
//  MainCoordinator.swift
//  FootballFinder
//
//  Created by manuel on 05/11/2020.
//

import Foundation
import UIKit
class MainCoordinator: Coordinator {
    var navigationController: UINavigationController    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    /// Function that show the first View
    func start() {
        let vc = MainViewController.instantiate()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }
    /// Function that show the DetailPlayer View
    /// - Parameter player: All the data of the player
    func goToDetailViewController(player: Player){
        let vc = DetailViewViewController.instantiate()
        vc.player = player
        navigationController.pushViewController(vc, animated: true)
    }
}
