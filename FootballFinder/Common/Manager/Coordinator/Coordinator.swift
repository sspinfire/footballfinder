//
//  Coordinator.swift
//  FootballFinder
//
//  Created by manuel on 05/11/2020.
//

import Foundation
import UIKit
protocol Coordinator {
    var navigationController: UINavigationController { get set }
    func start()
}
