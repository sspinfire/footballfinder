//
//  Constants.swift
//  FootballFinder
//
//  Created by manuel on 05/11/2020.
//

import Foundation

struct Constants {    
    struct Tokens {
        static let apiSport = "xaGm8sfWueCulRnrAmxI368vTAT1jiwdeuaJ7BiGVFIoEqZT2pKvSRyFLAYn"
    }
    
    struct URL {
        static let main = "https://soccer.sportmonks.com/api/v2.0/"
    }
    struct Endpoint {
        static let player = "players/search/"
        static let apiToken = "?api_token="
        static let team = "teams/"
    }
}
