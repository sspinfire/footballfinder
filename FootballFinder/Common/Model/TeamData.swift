//
//  TeamData.swift
//  FootballFinder
//
//  Created by manuel on 06/11/2020.
//

import Foundation
struct Team: Codable {
    let data: DataTeam
}
struct DataTeam: Codable {
    let name: String?
    let logo_path: String?
}
