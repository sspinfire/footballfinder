//
//  PlayerData.swift
//  FootballFinder
//
//  Created by manuel on 04/11/2020.
//

import Foundation
struct PlayerData: Codable {
    var data: [Player]
}
struct Player: Codable {
    let player_id: Int
    let team_id: Int?
    let country_id: Int?
    let position_id: Int?
    let common_name: String?
    let display_name: String?
    let fullname: String?
    let firstname: String?
    let lastname: String?
    let nationality: String?
    let birthdate: String?
    let birthcountry: String?
    let birthplace: String?
    let height: String?
    let weight: String?
    let image_path: String?
}
