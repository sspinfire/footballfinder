//
//  MainViewController.swift
//  FootballFinder
//
//  Created by manuel on 04/11/2020.
//

import UIKit

class MainViewController: UIViewController, CoordinatorDelegate {
    @IBOutlet private weak var searchBar: UISearchBar!
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var loandingActivity: UIActivityIndicatorView!
    var coordinator: MainCoordinator?
    let managerConnections = ManagerConnections()
    var players: [Player]?
    var objectDataSouce = ObjectDataSource()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configuration()
    }

    private func configuration() {
        self.title = "Football Finder"
        searchBar.delegate = self
        tableView.delegate = self
        tableView.dataSource = self.objectDataSouce
        loandingActivity.isHidden = true
        let cell = UINib(nibName: "PlayerTableViewCell", bundle: nil)
        self.tableView.register(cell, forCellReuseIdentifier: "PlayerCell")
        self.tableView.tableFooterView = UIView()
    }
    
    /// Function find the player and reload the tableView
    private func findPLayer(){
        managerConnections.listPlayer = { listPlayer in
            DispatchQueue.main.async {
                self.loandingActivity.isHidden = false
                self.loandingActivity.startAnimating()
                self.objectDataSouce.players = listPlayer
                self.tableView.reloadData()
            }
            return listPlayer
        }
    }
    
    /// Function empty the tableView
    private func emptyTable() {
        self.objectDataSouce.players = nil
        self.tableView.reloadData()
    }
}
extension MainViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
           if let lastVisibleIndexPath = tableView.indexPathsForVisibleRows?.last {
               if indexPath == lastVisibleIndexPath{
                loandingActivity.stopAnimating()
                loandingActivity.hidesWhenStopped = true
               }
           }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        coordinator?.goToDetailViewController(player: objectDataSouce.players![indexPath.row])
    }
}

extension MainViewController: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {emptyTable(); return}
        managerConnections.getDataPlayers(namePlayer: searchText)
        findPLayer()
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        emptyTable()
        searchBar.text = ""
    }
}
