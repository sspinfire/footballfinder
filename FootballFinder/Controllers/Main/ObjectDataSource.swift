//
//  ObjectDataSource.swift
//  FootballFinder
//
//  Created by manuel on 06/11/2020.
//

import Foundation
import UIKit
class ObjectDataSource: NSObject, UITableViewDataSource {
    var players: [Player]?
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let numPlayer = players?.count else { return 0}
        
        return numPlayer
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let player = players else { return UITableViewCell() }
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerCell") as! PlayerTableViewCell
        cell.name.text = player[indexPath.row].display_name
        cell.imagePlayer.image = getImage(imagePath: player[indexPath.row].image_path)
        cell.age.text = player[indexPath.row].birthdate
        return cell
    }
    
    /// Function that download an image
    /// - Parameter imagePath:Path url where is the image.
    /// - Returns: UIImage with the image downloaded
    private func getImage(imagePath: String?) -> UIImage{
        guard let strImage = imagePath else { return UIImage() }
        guard let url = URL(string: strImage) else{ return UIImage()}
        let data = try? Data(contentsOf: url)
        guard let image = UIImage(data: data!) else { return UIImage()}
        return image
    }
}
