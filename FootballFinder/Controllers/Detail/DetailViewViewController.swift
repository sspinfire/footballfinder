//
//  DetailViewViewController.swift
//  FootballFinder
//
//  Created by manuel on 06/11/2020.
//

import UIKit

class DetailViewViewController: UIViewController, CoordinatorDelegate, UITabBarDelegate, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    var player: Player?
    let objectDetailTableDataSource = ObjectDetailTableDataSource()
    override func viewDidLoad() {
        super.viewDidLoad()
        configuration()
    }
    
    private func configuration() {
        objectDetailTableDataSource.player = player
        tableView.delegate = self
        tableView.dataSource = self.objectDetailTableDataSource
        self.title = player?.display_name
        let cellData = UINib(nibName: "DataPlayerTableViewCell", bundle: nil)
        let cellImage = UINib(nibName: "DataImageTableViewCell", bundle: nil)
        self.tableView.register(cellData, forCellReuseIdentifier: "DataPlayerCell")
        self.tableView.register(cellImage, forCellReuseIdentifier: "DataImageCell")
    }

}
