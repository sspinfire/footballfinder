//
//  ObjectDetailTableDataSource.swift
//  FootballFinder
//
//  Created by manuel on 06/11/2020.
//

import Foundation
import UIKit
class ObjectDetailTableDataSource: NSObject, UITableViewDataSource {
    var player: Player?
    var managerConnections = ManagerConnections()
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DataImageCell") as! DataImageTableViewCell
            cell.dataImage.image = getImage(imagePath: player?.image_path)
            cell.dataLabel.text = player?.common_name
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DataPlayerCell") as! DataPlayerTableViewCell
            cell.dataPlayerLabel.text = player?.fullname
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DataImageCell") as! DataImageTableViewCell
            managerConnections.getDataTeam(idTeam: String((player?.team_id)!))
            managerConnections.team = {team in
                DispatchQueue.main.async {
                    cell.dataLabel.text = team.name
                    cell.dataImage.image = self.getImage(imagePath: team.logo_path)
                }
                return team
            }
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    /// Function that download an image
    /// - Parameter imagePath: Path url where is the image.
    /// - Returns: UIImage with the image downloaded
    private func getImage(imagePath: String?) -> UIImage{
        guard let strImage = imagePath else { return UIImage() }
        guard let url = URL(string: strImage) else { return UIImage() }
        let data = try? Data(contentsOf: url)
        guard let image = UIImage(data: data!) else { return UIImage()}
        return image
    }
}
