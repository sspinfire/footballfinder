//
//  PlayerTableViewCell.swift
//  FootballFinder
//
//  Created by manuel on 04/11/2020.
//

import UIKit

class PlayerTableViewCell: UITableViewCell {
    @IBOutlet weak var imagePlayer: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var age: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        age.text = getAge(birthDate: age.text)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    /// Function to calculate the age the player using the birthday date.
    /// - Parameter birthDate: the birthday date in string form in format "dd/MM/yyyy"
    /// - Returns: the age in string form
    private func getAge(birthDate: String?) -> String{
        guard birthDate != nil else { return "" }
        guard birthDate != "" else { return "" }
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "dd/MM/yyyy"
        let birthdayDate = dateFormater.date(from: birthDate!)
        let calendar: NSCalendar! = NSCalendar(calendarIdentifier: .gregorian)
        let now = Date()
        let calcAge = calendar.components(.year, from: birthdayDate!, to: now, options: [])
        guard let age = calcAge.year else{return ""}
        return String(age)
    }
}
