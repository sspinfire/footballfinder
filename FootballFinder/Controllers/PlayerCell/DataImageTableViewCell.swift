//
//  DataImageTableViewCell.swift
//  FootballFinder
//
//  Created by manuel on 05/11/2020.
//

import UIKit

class DataImageTableViewCell: UITableViewCell {

    @IBOutlet weak var dataImage: UIImageView!
    @IBOutlet weak var dataLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
